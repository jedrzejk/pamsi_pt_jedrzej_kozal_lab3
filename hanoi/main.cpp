#include "Stos_tablica.h"
#include <conio.h>
#include <windows.h>
#include <time.h>

using namespace std;

/*Idea jest taka:
mamy funckje przeloz
i 3 stosy z czego kazdy reprezentuje inny kolek
liczby int na stosie reprezentuja rozmiar kolka
zeby polozyc cos na kolku musisz uzyc metody top() i porownac z elementem ktory chcesz polozyc*/

//tablica na 3 stosy sa globalne, nie chce mi sie z tym bawic
stos<int> wieze[3];
/*
void czekaj(int sekundy)
{
	clock_t koniec_czekania;
	koniec_czekania = clock() + sekundy * CLOCKS_PER_SEC;
	while (clock() < koniec_czekania) {}
}

void wyswietl()
{
	char tablica[12][73];//tablica w ktorej przechowujemy obraz tego co sie dzieje, 3 x 21 - maksymalny rozmiar klocka,2 x 5 - przerwa miedzy tymi klockami
	for (unsigned int j = 0; j < 11; j++)
		for (unsigned int i = 0; i < 73; i++)
			tablica[j][i] = ' '; //inicjalizacja spacjami
	for (unsigned int i = 0; i < 73; i++)
		tablica[0][i] = '='; //oznaczenie ziemi
	for (unsigned int i = 1; i < 11; i++)
	{
		tablica[i][10]; //i oznaczenie slupkow
		tablica[i][36];
		tablica[i][62];
	}
	stos<int> tmp;//chwilowo trzeba bedzie zdjac wszystko ze stosu i wpisac do tego zeby miec w odwrotnej kolejnosci wszystkie elementy
	for (unsigned int i = 0; i < wieze[0].size(); i++)
		tmp.push_back(wieze[0].pop_back()); //przepisanie w odwrotnej kolejnosci
	for (unsigned int i = 0; i < tmp.size(); i++)
	{
		int rozmiar = tmp.pop_back(); //zdejmujemy z tmp po kolei
		for (unsigned j = 10 - rozmiar; j < 10-rozmiar+20; j++)
			tablica[i + 1][j] = '|'; //zeby wiedziec jak duzego klocka mamy wyswietlic
		wieze[0].push_back(rozmiar);//zwracamy na miejsce co zabralismy
	}
	tmp.clear();
	//teraz to samo dla drugiej wiezy
	for (unsigned int i = 0; i < wieze[1].size(); i++)
		tmp.push_back(wieze[1].pop_back());
	for (unsigned int i = 0; i < tmp.size(); i++)
	{
		int rozmiar = tmp.pop_back();
		for (unsigned j = 36 - rozmiar; j < 10 - rozmiar + 20; j++)
			tablica[i + 1][j] = '|';
		wieze[1].push_back(rozmiar);//zwracamy na miejsce co zabralismy
	}
	tmp.clear();
	for (unsigned int i = 0; i < wieze[2].size(); i++)
		tmp.push_back(wieze[2].pop_back());
	for (unsigned int i = 0; i < tmp.size(); i++)
	{
		int rozmiar = tmp.pop_back();
		for (unsigned j = 62 - rozmiar; j < 10 - rozmiar + 20; j++)
			tablica[i + 1][j] = '|';
		wieze[2].push_back(rozmiar);//zwracamy na miejsce co zabralismy
	}

	//wrescie wyswietlanie przygotowanej tablicy:
	for (int j = 12; j > -1; j--)
		for (int i = 0; i < 73; i++)
			cout << tablica[j][i];
	
	czekaj(10);
	cout << endl << endl;
}
*/

void wyswietl()
{
	for (unsigned int i = 0; i < 3; i++)
	{
		cout << "Wieza" << i + 1 << ":" << endl;
		wieze[i].wyswietl();
		cout << endl;
	}
	cout << endl;
}


/*z - z ktorego ma przelozyc
na - na ktore ma przelozyc*/
int przeloz(unsigned int z, unsigned int na)
{
	int tmp = wieze[z].pop_back(); //tymczasowe przechowanie zdjetego klocka
	if (!wieze[na].isEmpty()) //jesli jest pusta to nie ma po co sprawdzac czy mozemy polozyc element
	{
		if (tmp > wieze[na].top()) //sprawdzenie czy mozemy polozyc klocek
		{
			cerr << "Nie mozesz polozyc wiekszego klocka na mniejszym!" << endl;
			wieze[z].push_back(tmp); //odkladamy klocek zeby go nie zgubic
			return 1;
		}
	}
	wieze[na].push_back(tmp);
	return 0;
}

//rekurencyjna funkcja do wykonania zadania
//wieza1 - pierw - jest to wierze na ktorej sa poczatkowe klocki
//wieza2 - drug - wieza "buforowa"
//wieza3 - trzec - docelowa, na ktorej ma byc ilosc_klockow na koncu
//za pierwszym razem musi byc wywolywane bez parametrow domyslnych zeby dobrze sie ustawilo
void rozwiaz(unsigned int ilosc_klockow, unsigned int wieza1 = 0, unsigned int wieza2 = 1, unsigned int wieza3 = 2)
{
	if (ilosc_klockow == 1)
		przeloz(wieza1, wieza3);
	else
	{
		rozwiaz(ilosc_klockow - 1, wieza1, wieza3, wieza2); //przekladamy wszystkie klocki poza ostatnim z wiezy 1 na wieze2
		wyswietl();
		przeloz(wieza1, wieza3);//przekladamy najwiekszy klocek na ostatnia wieze
		wyswietl();
		rozwiaz(ilosc_klockow - 1, wieza2, wieza1, wieza3); //przekladamy z drugiego (bufforowego) na ostatni
		//wyswietl();
	}
}

void main()
{
	unsigned int ilosc_klockow;
	cout << "Podaj ilosc klockow: ";
		cin >> ilosc_klockow;
	for (unsigned int i = 0; i < ilosc_klockow; i++)
		wieze[0].push_back(ilosc_klockow-i);
	wyswietl();
	rozwiaz(ilosc_klockow);
	wyswietl();
	cout << "Koniec dzialania programu nacisnij dowolny przycisk aby zakonczyc" << endl;
	_getch();
}