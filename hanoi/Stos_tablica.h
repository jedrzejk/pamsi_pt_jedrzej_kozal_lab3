#include <iostream>

template <typename TYP>
class stos
{
	TYP *tablica; //ta tablica powinna byc danamicznie alokowana
	unsigned int MAX; //zmienna okreslajaca maksymalny rozmiar tablicy
	unsigned int koniec; //indeks konca

	//metody prywatne
	//inne strategie powiekszania tablicy
	int pow_o_stala(unsigned int c = 50); //zwieksza o stala c tablice
	int pow_dwa(); //zwieksza dwukrotnie rozmiar tablicy

public:
	//metody
	stos() { koniec = 0; MAX = 1; tablica = new TYP[MAX]; } //konstruktor
	~stos() { delete[MAX] tablica;} //destruktor

	void push_back(TYP arg);
	TYP pop_back();
	TYP top() {	return tablica[koniec-1];	}
	void clear() {	koniec = 0;	}

	unsigned int size() {   return koniec;   } //zwracamy indeks konca (ostatniego elementu)
	bool isEmpty(); //ta funkcja ma troche za duzo linijek zeby byc inline
	void wyswietl();
};


template <typename TYP>
bool stos<TYP>::isEmpty()
{
	if (size() == 0)
		return true;
	else
		return false;
}

template <typename TYP>
int stos<TYP>::pow_o_stala(unsigned int c = 50)
{
	//parametr c - rozmiar o jaki zwiekszamy tablice
	//tutaj musi byc alokacja nowej pamieci + przepisanie zmiennych + zwolnienie starej pamieci
	try
	{
		TYP *wsk = new TYP[MAX + c]; //alokacja nowej tablicy
		if (wsk == NULL)
			throw 1;
		for (unsigned int i = 0; i < MAX; i++)
			wsk[i] = tablica[i];
		delete[MAX] tablica; //usuniecie starej tablicy
		tablica = wsk; //przepisanie wskaznikow
		MAX += c; //zmiana rozmiaru tablicy
		return 0;
	}
	catch (int kod)
	{
		if (kod == 1)
		{
			std::cerr << "Nie mozna przydzielic pamieci!" << std::endl;
			return 1;
		}
	}
	return 2; //jestli cos pojdzie bardzo nie tak to zwracamy kod bledu
}

template <typename TYP>
int stos<TYP>::pow_dwa()
{
	//alokacja nowej pamieci 2 x wiekszej + przepisanie + usuniecie starej tablicy
	try
	{
		TYP *wsk = new TYP[MAX*2]; //alokacja nowej tablicy
		if (wsk == NULL)
			throw 1;
		for (unsigned int i = 0; i < MAX; i++)
			wsk[i] = tablica[i];
		delete[MAX] tablica; //usuniecie starej tablicy
		tablica = wsk; //przepisanie wskaznikow
		MAX *= 2; //zmiana rozmiaru tablicy
		return 0;
	}
	catch (int kod)
	{
		std::cerr << "Nie mozna przydzielic pamieci!" << std::endl;
		return 1;
	}
}


template <typename TYP>
void stos<TYP>::push_back(TYP arg)
{
	try
	{
		if (MAX == koniec) //jesli stos jest pelny to wyrzucamy wyjate
			throw 1; //FULLSTACKEXCEPTION
		else
		{
			tablica[koniec] = arg; //na ostatnim indeksie dodajemy nastepny element
			koniec++;
		}
	}
	catch (int blad) //obsulaga wyjatku pelny stos
	{
		if (blad == 1)
		{
			try //probujemy powiekszac tablice
			{
				//if(pow_dwa()==1)
				if (pow_o_stala() == 1)//jesli dostajemy kod bledu
					throw 2; //jak to sie nie uda to po prostu nie mozemy poprawnie przeprowadzic operacji push
				else //poprawny przebieg powiekszania
					push_back(arg);//jeszcze raz wywolujemy ta metode, zeby dodac ten element
			}
			catch (int kod) //wyswietlamy komunikat i konczymy dzialanie funckji
			{
				if (kod == 2)
				{
					std::cerr << "Nie mozna poprawnie przeprowadzic operacji push" << std::endl;
					return;
				}
			}
		}
	}
}

template <typename TYP>
TYP stos<TYP>::pop_back()
{
	try
	{
		if(isEmpty())
			throw 1; //EmptyStackException
		else
		{
			koniec--;
			return tablica[koniec];
		}
	}
	catch(int kod)
	{
		if (kod == 1)
		{
			std::cerr << "Nie mozna usuwac z pustego stosu" << std::endl;
			return 1;
		}
	}
	return 2;
}

template <typename TYP>
void stos<TYP>::wyswietl()
{
	if (isEmpty())
		std::cout << "Stos jest pusty" << endl;
	else
		for (unsigned int i = 0; i < koniec; i++)
			std::cout << tablica[i] << "\t";
}