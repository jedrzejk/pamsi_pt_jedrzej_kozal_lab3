#include <queue>
#include <iostream>

//klasa do kolejki z STL
template<typename TYP>
class STLkolejka
{
	std::queue<TYP> kolejka;
public:

	void push(TYP elem) { kolejka.push(elem); }
	TYP pop() {	TYP tmp = kolejka.front(); kolejka.pop(); return tmp;	}  //przepisanie i usuniecie pierwszego elementu
	void clear() { while (!kolejka.empty()) { kolejka.pop(); } } //usuwanie wszystkich elementow po kolei
	void wyswietl();
	STLkolejka() {};
	~STLkolejka() { clear(); } //zwolnienie zajmowanej pamieci w destruktorze
};

template <typename TYP>
void STLkolejka<TYP>::wyswietl()
{
	std::queue<TYP> tmp;
	TYP tmp1;
	std::cout << "Kolejka zawiera nastepujace elementy:" << endl;
	while (!kolejka.empty())
	{
		tmp1 = pop();//zdjecie elementu
		std::cout << tmp1 << std::endl;
		tmp.push(tmp1);
	}
	kolejka = tmp;
}