#include <iostream>

template <typename TYP>
class kolejka_tab
{
	TYP *tablica; //ta tablica powinna byc danamicznie alokowana
	unsigned int MAX; //zmienna okreslajaca maksymalny rozmiar tablicy
	unsigned int poczatek; //indeks poczatku
	unsigned int koniec; //indeks konca

						 //metody prywatne
						 //inne strategie powiekszania tablicy
	int pow_o_stala(unsigned int c = 50); //zwieksza o stala c tablice
	int pow_dwa(); //zwieksza dwukrotnie rozmiar tablicy

public:
	//metody
	kolejka_tab() { poczatek = 0; koniec = 0; MAX = 1; tablica = new TYP[MAX]; } //konstruktor
	~kolejka_tab() { delete[MAX] tablica; } //destruktor

	void push_back(TYP arg);
	TYP pop_front();
	TYP front() { return tablica[poczatek]; }
	void clear() { poczatek = 0;  koniec = 0; }

	unsigned int size() 
	{ 
		if (MAX == 1 && koniec == 1)	return 1;
		else
		{
			if (koniec == poczatek)	return 0;
			else //tutaj mamy pewnosc ze na pewno nie powinnismy zwrocic zera
			{
				unsigned tmp = (MAX - poczatek + koniec) % MAX;
				if (tmp == 0)//tutaj juz wiemy ze to nie jest zero, a np 4 modulo 2 jest zero, a przy max 2 powinnismy zwrocic 2
					return MAX;
				else
					return tmp;
			}
		}
	} 
	bool isEmpty() { return static_cast<bool>(poczatek == koniec); } // w teorii jesli size() zwroci zero to bedzie to nam ladnie rzutowalo na bool
	void wyswietl();
};


template <typename TYP>
int kolejka_tab<TYP>::pow_o_stala(unsigned int c = 50)
{
	//parametr c - rozmiar o jaki zwiekszamy tablice
	//tutaj musi byc alokacja nowej pamieci + przepisanie zmiennych + zwolnienie starej pamieci
	try
	{
		TYP *wsk = new TYP[MAX + c]; //alokacja nowej tablicy
		if (wsk == NULL)
			throw 1;
		else
		{
			unsigned int i;
			for (i = 0; i < MAX; i++)
				wsk[i] = tablica[i + poczatek]; //przepisujemy na poczatek tablicy
			if (poczatek > koniec) //jesli koniec przeszedl do poczatku tablicy
				for (unsigned int j = 0; i < koniec; i++, j++)//to musimy do nowej tablicy dopisac to z poczatku
					wsk[i] = tablica[j];

			delete[MAX] tablica; //usuniecie starej tablicy
			tablica = wsk; //przepisanie wskaznikow
			MAX += c; //zmiana rozmiaru tablicy
			return 0;
		}
	}
	catch (int kod)
	{
		if (kod == 1)
		{
			std::cerr << "Nie mozna przydzielic pamieci!" << std::endl;
			return 1;
		}
	}
	return 2; //jestli cos pojdzie bardzo nie tak to zwracamy kod bledu
}

template <typename TYP>
int kolejka_tab<TYP>::pow_dwa()
{
	//parametr c - rozmiar o jaki zwiekszamy tablice
	//tutaj musi byc alokacja nowej pamieci + przepisanie zmiennych + zwolnienie starej pamieci
	try
	{
		TYP *wsk = new TYP[MAX*2]; //alokacja nowej tablicy
		//if (wsk == NULL)
			//throw 1;
		//else
		//{
			unsigned int i;
			for (i = 0; i < MAX; i++)
				wsk[i] = tablica[i + poczatek]; //przepisujemy na poczatek tablicy
			if (poczatek > koniec) //jesli koniec przeszedl do poczatku tablicy
				for (unsigned int j = 0; i < koniec; i++, j++)//to musimy do nowej tablicy dopisac to z poczatku
					wsk[i] = tablica[j];

			delete[MAX] tablica; //usuniecie starej tablicy
			tablica = wsk; //przepisanie wskaznikow
			MAX *= 2; //zmiana rozmiaru tablicy
			return 0;
		//}
	}
	catch (int kod)
	{
		if (kod == 1)
		{
			std::cerr << "Nie mozna przydzielic pamieci!" << std::endl;
			return 1;
		}
	}
	return 2; //jestli cos pojdzie bardzo nie tak to zwracamy kod bledu
}

template <typename TYP>
void kolejka_tab<TYP>::push_back(TYP arg)
{
	try
	{
		if (MAX == size()) //jesli kolejka_tab jest pelny to wyrzucamy wyjate
			throw 1; //FULLSTACKEXCEPTION
		else
		{
			tablica[koniec] = arg; //na ostatnim indeksie dodajemy nastepny element
			koniec++;
			if (koniec == MAX && poczatek > 0)//jesli koniec wyjdzie poza tablice to dajemy go na poczatek tablicy, bo jeszcze jest miejsce
				koniec = 0;

		}
	}
	catch (int blad) //obsulaga wyjatku pelny kolejka_tab
	{
		if (blad == 1)
		{
			try //probujemy powiekszac tablice
			{
				//if (pow_o_stala() == 1)//jesli dostajemy kod bledu
				if (pow_dwa() == 1)
					throw 2; //jak to sie nie uda to po prostu nie mozemy poprawnie przeprowadzic operacji push
				else //poprawny przebieg powiekszania
					push_back(arg);//jeszcze raz wywolujemy ta metode, zeby dodac ten element
				return;
			}
			catch (int kod) //wyswietlamy komunikat i konczymy dzialanie funckji
			{
				if (kod == 2)
				{
					std::cerr << "Nie mozna poprawnie przeprowadzic operacji push" << std::endl;
					return;
				}
			}
		}
	}
}

template <typename TYP>
TYP kolejka_tab<TYP>::pop_front()
{
	try
	{
		if (isEmpty())
			throw 1; //EmptyStackException
		else
		{
			if (poczatek < MAX-1) //trzeba sprawdzac czy nie trzeba wrocic na poczatek tablicy z konca
			{
				poczatek++;
				return tablica[poczatek - 1];
			}
			else
			{
				poczatek = 0;
				return tablica[MAX - 1];
			}
		}
	}
	catch (int kod)
	{
		if (kod == 1)
		{
			std::cerr << "Nie mozna usuwac z pustego kolejka_tabu" << std::endl;
			return 1;
		}
	}
	return 2;
}

template <typename TYP>
void kolejka_tab<TYP>::wyswietl()
{
	if (isEmpty())
		std::cout << "kolejka jest pusta" << endl;
	else
	{
		unsigned int granica;
		if (poczatek > koniec)
			granica = MAX;
		else
			granica = koniec;
		for (unsigned int i = poczatek; i < granica; i++)
			std::cout << tablica[i] << std::endl;
		if (poczatek > koniec) //jesli kolejka jest zawianieta
			for (unsigned int i = 0; i < koniec; i++) //to musimy wyswietlic jeszcze nastepna czesc
				std::cout << tablica[i] << std::endl;
	}
}