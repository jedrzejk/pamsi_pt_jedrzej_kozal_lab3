#include "Stos_tablica.h"
#include <conio.h>

using namespace std;
/*
void main()
{
	stos<int> s;
	if (s.isEmpty())
		cout << "Stos jest pusty" << endl;
	else
		cout << "Nie jest pusty D:" << endl;
	for (int i = 0; i < 10; i++)
		s.push_back(i);
	s.wyswietl();
	s.pop_back();
	s.wyswietl();
	cout << "Nacisnij dowolny klawisz aby zakonczyc" << endl;
	_getch();
}*/

void main()
{
	stos<int> s;
	int wybor;
	int wprowadzona;
	cout << "STOS" << endl;
	do
	{
		cout << "Menu" << endl;
		cout << "1 - Dodaj element" << endl;
		cout << "2 - Usun element" << endl;
		cout << "3 - Wyswietl wszystko" << endl;
		cout << "4 - Wyswietl rozmiar stosu" << endl;
		cout << "5 - Usun wszystkie elementy" << endl;
		cout << "6 - Zakoncz" << endl;
		cout << "\nTwoj wybor: ";
		cin >> wybor;
		switch (wybor)
		{
		case 1:
			cout << "Podaj element ktory mam dodac: ";
			cin >> wprowadzona;
			s.push_back(wprowadzona);
			break;
		case 2:
			cout << "Zdjety element to " << s.pop_back() << endl;
			break;
		case 3:
			s.wyswietl();
			break;
		case 4:
			cout << "Stos zawiera " << s.size() << " elementow." << endl;
			break;
		case 5:
			s.clear();
			cout << "Usunieto wszystkie elementy" << endl;
			break;
		case 6:
			break;
		default:
			cout << "Wprowadz jedna z podanych liczb" << endl;
		}
	} while (wybor != 6);
}