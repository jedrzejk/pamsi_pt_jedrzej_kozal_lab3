#include <stack>
#include <iostream>

//klasa do kolejki z STL
template<typename TYP>
class STLstos
{
	std::stack<TYP> stos;
public:

	void push(TYP elem) { stos.push(elem); }
	TYP pop() { TYP tmp = stos.top(); stos.pop(); return tmp; }  //przepisanie i usuniecie ostatniego elementu
	void clear() { while (!stos.empty()) { stos.pop(); } } //usuwanie wszystkich elementow po kolei
	void wyswietl();
	STLstos() {};
	~STLstos() { clear(); } //zwolnienie zajmowanej pamieci w destruktorze
};

template <typename TYP>
void STLstos<TYP>::wyswietl()
{
	std::queue<TYP> tmp;
	TYP tmp1;
	std::cout << "stos zawiera nastepujace elementy:" << endl;
	while (!stos.empty())
	{
		tmp1 = pop();//zdjecie elementu
		std::cout << tmp1 << std::endl;
		tmp.push(tmp1);
	}
	stos = tmp.emplace(); //przepisanie odwroceonego stosu
}